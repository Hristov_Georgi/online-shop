<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('ajax-deliveries', [DeliveryController::class,'ajaxIndex']);
Route::post('ajax-deliveries/{delivery}',[DeliveryController::class,'ajaxDestroy']);

Route::get('ajax-products', [ProductController::class,'ajaxIndex']);
Route::post('ajax-product/{product}',[ProductController::class,'ajaxDestroy']);


Route::get('activity-log', ActivityController::class)->name('activityLog');
Route::put('deliveries/emei/{delivery}',[DeliveryController::class,'addImei'])->name('deliveries.products.imeis');
Route::delete('deliveries/clear/{delivery}',[DeliveryController::class,'clearProducts'])->name('deliveries.products.clear');
Route::get('deliveries/pdf/{delivery}',[DeliveryController::class,'generatePDF'])->name('deliveries.generatePDF');
Route::resource('deliveries',DeliveryController::class);
Route::resource('products',ProductController::class);


Route::get('users', [UserController::class,'index']);               //test
Route::post('delete/{id}', [UserController::class, 'destroy']);     //test



