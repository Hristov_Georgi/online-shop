@extends('layout')

@section('content')    

    @if(Session::has('message'))
    <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif

    <h1>Create product</h1>
    <form action="{{ route('products.store') }}" method="POST">
        @csrf
        <p>Product Name</p>
        <input type="text" name="name" >
        <p>Price</p>
        <input type="number" name="price">
        <p>
            <button type="submit">Create</button>
        </p>
    </form>

@endsection