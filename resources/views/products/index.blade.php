@extends('layout')


@section('content')

    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif


    <h2>All Products</h2>

    <p><a href="{{ route('products.create') }}">Create Product</a></p>


    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Product</th>
            <th scope="col">Price</th>
            <th scope="col">Quantity</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
         
          @foreach ($products as $index => $product)
            <tr>
                <th scope="row">{{ ++$index }}</th>
                <td>{{ strtoupper($product->name) }}</td>
                <td>${{ $product->price }}</td>
                <th>{{ $product->totalQuantity()}}</th>
                <td>
                    <form method="get" action="{{ route('products.edit',$product->id) }}">
                        <button type="submit">EDIT</button>
                    </form>
                </td>
                <td>
                    <form method="POST" action="{{ route('products.destroy',$product->id) }}">
                        @csrf
                        @method('delete')

                        <button type="submit">Delete</button>

                    </form>
                </td>
            </tr>      
          @endforeach
          
        </tbody>
      </table>

   
@endsection

