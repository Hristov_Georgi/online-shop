@extends('layout')

@section('content')

@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif

<h2>Product Details</h2>

<p>Product</p>
<h3>{{$product->name }}</h3>

<p>Price</p>
<h3>{{$product->price }}</h3>

@endsection