@extends('layout')


@section('content')


    @foreach ($errors->all() as $message)
        <p>{{ $message }}</p>
    @endforeach

    <h2>Edit Product</h2>

    <form method="POST" action="{{ route('products.update',$product->id) }}">
        @csrf
        @method('put')

        <p>Name</p>
        <input type="text" name="name" value="{{ $product->name }}">
        <p>Price</p>
        <input type="number"name="price" value="{{ $product->price }}">

        <p>
            <button type="submit">Edit</button>
        </p>
    </form>


@endsection