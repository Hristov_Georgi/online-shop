@extends('layout')

@section('content')

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    {{-- <script src="{{ asset("/js/app.js") }}" defer></script> --}}
@endpush
    


<h2>All Products</h2>

<p><a href="{{ route('products.create') }}">Create Product</a></p>


<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Product</th>
        <th scope="col">Price</th>
        <th scope="col">Quantity</th>
        <th scope="col">Edit</th>
        <th scope="col">Delete</th>
      </tr>
    </thead>
    <tbody>
     
      @foreach ($products as $index => $product)
        <tr id="{{ $product->id }}">
            <th scope="row">{{ ++$index }}</th>
            <td>{{ strtoupper($product->name) }}</td>
            <td>${{ $product->price }}</td>
            <th>{{ $product->totalQuantity()}}</th>
            <td>
                <form method="get" action="{{ route('products.edit',$product->id) }}">
                    <button type="submit">EDIT</button>
                </form>
            </td>
            <td>
                <button class="btn btn-danger" onclick="deleteConfirmation({{$product->id}})">Delete</button>
            </td>
        </tr>      
      @endforeach
      
    </tbody>
  </table>


    {{-- <script src="{{ asset("/js/products/delete.js") }}"></script> --}}
    <script>
        function deleteConfirmation(id) {
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (e) {
                
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    
                    $.ajax({
                        type: 'POST',
                        url: "{{url('/ajax-product')}}/" + id,
                        data: {_token: CSRF_TOKEN},
                        dataType: 'JSON',
                        success:function (results){
                        if (results) {
                            Swal.fire(
                            'Deleted!',
                            'Your prodcut has been deleted.',
                            'success'
                            )
                            $( "tr" ).remove( "#"+id );
                            }
                        }
                    });
                } else {
                    e.dismiss;
                }

            }, function (dismiss) {
                return false;
            })
        }
    </script>

@endsection