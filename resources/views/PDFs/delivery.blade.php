
     <div>
        <h5>Created On:{{ $delivery->created_at}}</h5>
        <h5>Delivery ID:{{ $delivery->id}}</h5>
    </div>

    <table class="table">
    
        <thead>
        <tr>
            <th scope="col">Products</th>
            <th scope="col">Price</th>
            <th scope="col">Qty</th>
            <th scope="col">Sum</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($delivery->products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->pivot->qty }}</td>
                    <td>{{ $product->pivot->qty*$product->price }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <td>Total: {{ $delivery->total() }}</td>
            <td>Total: {{ $delivery->total }}</td>
        </tfoot>
    </table>
    




