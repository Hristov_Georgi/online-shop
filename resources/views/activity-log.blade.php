@extends('layout')


@section('content')
<H1>Activity log</H1>


    <table class="table">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Model</th>
        <th scope="col">Activity description</th>
        <th scope="col">Properties</th>
        <th scope="col">Occurence Date</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($logs as $key => $log)
        <tr>
            <th scope="row">{{ ++$key}}</th>
            <td>{{ substr($log->subject_type,1 + strrpos($log->subject_type,'\\')) }}</td>
            <td>{{ strtoupper($log->description) }} </td>
            <td>{{ $log->properties }}</td>
            <td>{{ $log->created_at->diffForHumans() }}</td>
        </tr>
        @endforeach
    </tbody>
    </table>


    {{ $logs->links() }}
@endsection