
@extends('layout')

@section('content')

    <h1>Crate delivery</h1>

    @if(Session::has('message'))
    <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif

    <form action="{{ route('deliveries.store') }}" method="POST">
        @csrf
        <button type="submit">Create a new one</button>
    </form>
    
@endsection
