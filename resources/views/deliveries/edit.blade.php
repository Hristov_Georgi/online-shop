@extends('layout')

@section('content')


    
    <h2>Edit Delivery</h2>

    @if(Session::has('message'))
    <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif

    @foreach ($errors->all() as $message)
    <p class="alert alert-danger">{{ $message }}</p>
    @endforeach


    <div class="row row-list">
    
        

        <form method="POST" class="col-xs-3" action="{{ route('deliveries.update',$delivery) }}">
            @csrf
            @method('PUT')
            <select name="products[]" multiple>
                @foreach ($products as $product)
                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                @endforeach
            </select> 
            
            <div>
                <p>Select quantity</p>
                <input name="qty" type="number">
            </div>
            <p>
                <button type="submit">Add to Order</button>
            </p>
        </form>
        
        <div class="col-xs-3 ">
            <p style="display: inline-block">Current Status is </p>

            @if ($delivery->is_pending)
                <span>pending!</span>
            @else
                <span>completed!</span>
            @endif

            @if (count($delivery->products))
            <h3>Currently Added Products</h3>
            
            <form action="{{ route('deliveries.products.clear',$delivery->id) }}" method="POST">
                @csrf
                @method('DELETE')
                @forelse ($delivery->products as $prod)
                    <p>{{$prod->name}} - {{ $prod->pivot->qty}} </p> 
                @empty
                    <p>Empty so far!</p>
                @endforelse

                <button type="submit">CLEAR PRODUCTS BASKET</button>
            </form>
            @endif
           
        </div>
    
        @if (count($delivery->products))
        <div class="col-xs-3 ">
            <h3>Add EMEIS</h3>
            <form action="{{ route('deliveries.products.imeis',$delivery) }}" method="POST">
                @csrf
                @method('PUT')
                @forelse ($delivery->products as $prod)

                <p>Product {{ $prod->name }}: {{ $prod->pivot->qty }} </p>
                
                <ul>
                    @for ($i = 0; $i < $prod->pivot->qty; $i++)
                    <li><input type="text" name="imei[][{{$prod->name}}]" placeholder="Enter IMEI"></li> 
                    <!-- <li><input type="text" name="{{$prod->name}}[]" placeholder="Enter IMEI"></li>   -->
                    @endfor
                </ul>
                @empty
                @endforelse

                <button type="submit">Add and Complete Delilvery</button>
            </form>
        </div>
        @endif
       
    </div>
@endsection