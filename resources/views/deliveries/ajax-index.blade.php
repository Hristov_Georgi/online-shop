@extends('layout')

@section('content')

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
@endpush
    

<table class="table">
    <thead>
      <tr>
          <th scope="col">Delivery #</th>
          <th scope="col">Date</th>
          <th scope="col">Status</th>
          <th scope="col">Edit</th>
          <th scope="col">Delete</th>
      </tr>
    </thead>

    <tbody>
     
      @foreach ($deliveries as $key => $delivery)
        <tr id="{{ $delivery->id }}">
            <td>{{ ++$key}}</td>
            <td>{{ $delivery->created_at }}</td>
            @if ($delivery->is_pending)
                <td>Pending / Not Completed</td>
            @else
                <td>Completed</td>
            @endif

            @if (!$delivery->is_pending)
            
            @else

            @endif
            <td>
              @if (!$delivery->is_pending)
                <button><a href="{{ route('deliveries.generatePDF',$delivery->id) }}">PDF</a></button>
              @else
                <button><a href="{{ route('deliveries.edit',$delivery->id) }}">EDIT</a></button>
              @endif
            </td>
            <td>
              <button class="btn btn-danger" onclick="deleteConfirmation({{$delivery->id}})">Delete</button>
          </td>
        </tr>    
      @endforeach
      
    </tbody>
  </table>


  {{-- <script src="{{ asset("/js/deliveries/delete.js") }}"></script> --}}
  <script>

    function deleteConfirmation(id) {
        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (e) {
            
            if (e.value === true) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                
                $.ajax({
                    type: 'POST',
                    url: "{{url('/ajax-deliveries')}}/" + id,
                    data: {_token: CSRF_TOKEN},
                    dataType: 'JSON',
                    success:function (results){
                    if (results) {
                        Swal.fire(
                        'Deleted!',
                        'Your delivery has been deleted.',
                        'success'
                        )
                        $("tr").remove("#"+id)
                        }
                    }
                });

            } else {
                e.dismiss;
            }

        }, function (dismiss) {
            return false;
        })
    }
  </script>

@endsection