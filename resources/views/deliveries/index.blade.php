@extends('layout')


@section('content')



@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif

<form action="{{ route('deliveries.store') }}" method="POST">
  @csrf
  <button type="submit">Create New Delivery</button>
</form>
{{-- <a href="{{ route('deliveries.create') }}">Create New Delivery</a> --}}

<table class="table">
    <thead>
      <tr>
          <th scope="col">Delivery #</th>
          <th scope="col">Date</th>
          <th scope="col">Status</th>
          <th scope="col">Edit</th>
          <th scope="col">Delete</th>
      </tr>
    </thead>
    <tbody>
     
      @foreach ($deliveries as $key => $delivery)
        <tr>
            <td>{{ ++$key}}</td>
            <td>{{ $delivery->created_at }}</td>
            @if ($delivery->is_pending)
                <td>Pending / Not Completed</td>
            @else
                <td>Completed</td>
            @endif

            @if (!$delivery->is_pending)
            
            @else

            @endif
            <td>
              @if (!$delivery->is_pending)
                <button><a href="{{ route('deliveries.generatePDF',$delivery->id) }}">PDF</a></button>
              @else
                <button><a href="{{ route('deliveries.edit',$delivery->id) }}">EDIT</a></button>
              @endif
            </td>
            <td>
              <form action="{{ route('deliveries.destroy',$delivery->id) }}" method="POST">
                @csrf
                @method('delete')
                <button type="sumbmit">DELETE</button>
              </form>
          </td>
        </tr>    
      @endforeach
      
    </tbody>
  </table>


@endsection