<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Product extends Model
{
    use HasFactory, LogsActivity;

    protected static $logAttributes = ['name', 'price'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Product has been {$eventName}";
    }
    
    protected static $logOnlyDirty = true;
    
    protected $guarded = [];

    public function deliveries()
    {
        return $this->belongsToMany(Delivery::class)->withPivot('qty');
    }

    public function allImeis()
    {
        return $this->hasMany(Imei::class);
    }




    public function totalQuantity()
    {
        return count($this->allImeis);
    }

    public function totalSum()
    {
        return $this->totalQuantity()*$this->price;
    }

}
