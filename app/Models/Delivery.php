<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Delivery extends Model
{
    use HasFactory,LogsActivity;

    protected static $logAttributes = ['total', 'is_pending'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Delivery has been {$eventName}";
    }

    protected static $logOnlyDirty = true;

    protected $guarded = [];


    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('qty');
    }


    public function total()
    {
        return $this->products->sum(function ($product) {
            return $product['price']*$product->pivot->qty;
        });
    }



    public function imeis()
    {
        return $this->hasManyThrough(Imei::class,Product::class,'id');
        // return $this->hasManyThrough(Imei::class,DeliveryProduct::class);
    }




}
