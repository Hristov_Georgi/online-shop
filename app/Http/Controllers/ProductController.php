<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::all();

        return view('products.index',compact('products'));
    }

    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }


    public function create()
    {
        return view('products.create');
    } 



    public function store(StoreProductRequest $request)
    {

        $validated = $request->validated();

        Product::create([
            'name' => $validated['name'],
            'price' => $validated['price'], 
        ]);

        return back()->with('message','product added successuly');

    }

    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        
        $params = $request->validated();

        $product->name = $params['name'];
        $product->price = $params['price'];
        $product->update();


        return redirect()->route('products.show',$product)->with('message','edit successful');
    }

    public function destroy(Product $product)
    {
         $product->delete();

         return redirect()->route('products.index')->with('message',' product successfuly deleted');

    }

    public function ajaxIndex()
    {
        $products = Product::latest()->get();
        return view('products.ajax-index', compact('products'));
    }

    public function ajaxDestroy(Product $product)
    {

        
        $delete = $product->delete();

        // check data deleted or not
        if ($delete == 1) {
            $success = true;
            $message = "Product deleted successfully";
        } else {
            $success = true;
            $message = "Product not found";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);

    }

}
