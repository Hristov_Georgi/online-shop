<?php

namespace App\Http\Controllers;

use App\Models\Imei;
use App\Models\Product;
use App\Models\Delivery;
use App\Http\Requests\Delivery\DeliveryUpdateRequest;
use App\Http\Requests\Delivery\DeliveryAddEmeiRequest;
use Illuminate\Support\Facades\DB;
use PDF;
use Spatie\Activitylog\Models\Activity;

class DeliveryController extends Controller
{

    public function index()
    {
        $deliveries = Delivery::all();
        return view('deliveries.index', compact('deliveries'));
    }

    public function create()
    {
        return view('deliveries.create');
    }

    public function edit(Delivery $delivery)
    {
        $products = Product::all();
        $imeis = Imei::all();               
        return view('deliveries.edit', compact(['delivery', 'products','imeis']));
    }

    public function store()
    {
        $delivery = new Delivery();
        $delivery->save();

        return back()->with("message", 'New Delivery Order Created');
    }

    public function update(DeliveryUpdateRequest $request, Delivery $delivery)
    {
        $products = $request->all()['products'];
        $qty = $request->all()['qty'];

        foreach ($products as $productId) {
            $delivery->products()->attach(Product::find($productId), ['qty' => $qty]);
        }

        return back()->with("message", sizeof($products) > 1 ? 'Products Added' : 'Product Added');
    }

    public function clearProducts(Delivery $delivery)
    {
        $ids = $delivery->products->pluck('id');

        $delivery->products()->detach($ids);

        return back()->with('message','Cart Cleared');
    }


    public function addImei(DeliveryAddEmeiRequest $request, Delivery $delivery)
    {
       
        $imeis = $request->all()['imei'];

        foreach ($imeis as $pair) {
            foreach ($pair as $productName => $imei) {
                
                $productId = Product::where('name',$productName)->pluck('id')[0];
                
                Imei::create([
                    'imeis' => $imei,
                    'product_id' => $productId,
                    ]);
                }
            }
            

        // change delivery status
        $delivery->is_pending = false;
        $delivery->total = $delivery->total();
        $delivery->save();
        
        return redirect()->route('deliveries.index')->with('message', sizeof($imeis)>1? 'Imeis Added And Delivery Completed' : 'Imei Added And Delivery Completed');
    }


    public function destroy(Delivery $delivery)
    {
        Delivery::destroy($delivery->id);

        return back()->with('message', 'Delivery Deleted');
    }

    public function generatePDF(Delivery $delivery)
    {  
        return PDF::loadView('PDFs.delivery', ['delivery' => $delivery])->stream();
    }





    public function ajaxIndex()
    {
        $deliveries = Delivery::latest()->get();
        return view('deliveries.ajax-index',['deliveries' => $deliveries]);
    }

    public function ajaxDestroy(Delivery $delivery)
    {

        $delete = $delivery->delete();

        // check data deleted or not
        if ($delete == 1) {
            $success = true;
            $message = "Delivery deleted successfully";
        } else {
            $success = true;
            $message = "Delivery not found";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);

    }


}
