function deleteConfirmation(id) {
    swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (e) {
        
        if (e.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            
            $.ajax({
                type: 'POST',
                url: "{{url('/ajax-deliveries')}}/" + id,
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                success:function (results){
                if (results) {
                    Swal.fire(
                    'Deleted!',
                    'Your delivery has been deleted.',
                    'success'
                    )
                    $("tr").remove("#"+id)
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}